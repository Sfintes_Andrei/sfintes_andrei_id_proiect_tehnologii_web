import React, { Component } from 'react'
import ApiService from "../../service/ApiService";
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Button from '@material-ui/core/Button';
import CreateIcon from '@material-ui/icons/Create';
import DeleteIcon from '@material-ui/icons/Delete';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';

class ListComponent extends Component {

    constructor(props) {
        super(props)
        this.state = {
            document: [],
            message: null,
            filterExp:''
        }
        this.delete = this.delete.bind(this);
        this.edit = this.edit.bind(this);
        this.add = this.add.bind(this);
        this.reloadList = this.reloadList.bind(this);
    }

    componentDidMount() {
        this.reloadList();
    }

    reloadList() {
        ApiService.fetch()
            .then((res) => {
                this.setState({document: res.data})
            });
    }

    delete(docId) {
        let doc = {id: docId};
        ApiService.delete(doc)
           .then(res => {
               this.setState({message : 'Deleted successfully.'});
               this.setState({users: this.state.document.filter(doc => doc.id !== docId)});
           })
        this.props.history.push('/list');
    }

    edit(id,name,comment) {
        window.localStorage.setItem("id", id);
        window.localStorage.setItem("name", name);
        window.localStorage.setItem("comment", comment);
        this.props.history.push('/edit');
    }

    add() {
        window.localStorage.removeItem("userId");
        this.props.history.push('/add');
    }




 onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    render() {
        return (
            <div>
                <Typography variant="h4" style={style}>Details</Typography>
                <Button variant="contained" color="primary" onClick={() => this.add()}>
                    Add
                </Button>

                <p></p>

                <Table  options={{search: true}}>
                    <TableHead>
                        <TableRow>
                            <TableCell>Id</TableCell>
                            <TableCell align="right">Nume</TableCell>
                            <TableCell align="right">Uid</TableCell>
                            <TableCell align="right">Comment</TableCell>

                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.state.document.map(row => (
                            <TableRow key={row.id}>
                                <TableCell component="th" scope="row">
                                    {row.id}
                                </TableCell>
                                <TableCell align="right">{row.name}</TableCell>
                                <TableCell align="right">{row.uid}</TableCell>
                                <TableCell align="right">{row.comment}</TableCell>
                                <TableCell align="right" onClick={() => this.edit(row.id,row.name,row.comment)}><CreateIcon /></TableCell>
                                <TableCell align="right" onClick={() => this.delete(row.id)}><DeleteIcon /></TableCell>

                            </TableRow>
                        ))}
                    </TableBody>

                </Table>

            </div>
        );
    }

}

const style ={
    display: 'flex',
    justifyContent: 'center'
}

export default ListComponent;