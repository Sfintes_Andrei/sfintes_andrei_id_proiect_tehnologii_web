
var express= require("express");
var fetch=require("./fetch/fetch");
var bodyparser= require("body-parser");


var app=express();
const cors = require('cors');
app.use(cors());
app.options('*', cors());

app.use("/fetch",fetch);


app.use(bodyparser.json());


app.use(bodyparser.urlencoded({extended:false}));

var insert = require("./insert/insert");
app.use("/insert",insert);


var update= require("./update/update");
app.use("/update",update);


var remove=require("./delete/delete");
app.use("/delete",remove);


var upload=require("./upload/fileupload");
app.use("/file/upload",upload);


app.listen(8080);
console.log("server is listening at port no. 8080");