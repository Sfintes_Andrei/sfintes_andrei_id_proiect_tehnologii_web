CREATE TABLE `documents` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `comment` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `uid` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE `test`.`document_audits` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `lastchangetime` DATETIME NULL,
  `documentId` INT NULL,
  PRIMARY KEY (`id`),
  CONSTRAINT `iddocument_fk`
    FOREIGN KEY (`documentId`)
    REFERENCES  documents (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE) ENGINE=MyISAM AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;