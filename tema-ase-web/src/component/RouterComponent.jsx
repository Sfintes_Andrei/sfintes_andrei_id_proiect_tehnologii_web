import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import ListComponent from "./user/ListComponent";
import AddComponent from "./user/AddComponent";
import FileUploadComponent from "./user/FileUploadComponent";
import EditComponent from "./user/EditComponent";
import React from "react";

const AppRouter = () => {
    return(
        <div style={style}>
            <Router>
                    <Switch>
                        <Route path="/" exact component={ListComponent} />
                        <Route path="/list" component={ListComponent} />
                        <Route path="/add" component={AddComponent} />
                        <Route path="/upload" component={FileUploadComponent} />
                        <Route path="/edit" component={EditComponent} />

                    </Switch>
            </Router>
        </div>
    )
}

const style={
    marginTop:'20px'
}

export default AppRouter;