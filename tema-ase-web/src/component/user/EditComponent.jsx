import React, {Component} from 'react'
import ApiService from "../../service/ApiService";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

class EditComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            id: '',
            name: '',
            comment: ''
        }
        this.save = this.save.bind(this);
        this.load = this.load.bind(this);
    }

    componentDidMount() {
        this.load();
    }

    load() {

        this.setState({
            id: window.localStorage.getItem("id"),
            name: window.localStorage.getItem("name"),
            comment: window.localStorage.getItem("comment")
        })

    }

    onChange = (e) =>
        this.setState({[e.target.name]: e.target.value});

    save = (e) => {
        e.preventDefault();
        let doc = {id: this.state.id, name: this.state.name, comment: this.state.comment};
        ApiService.edit(doc)
            .then(res => {
                this.setState({message: 'Added successfully.'});
                this.props.history.push('/list');
            });
    }

    render() {
        return (
            <div>
                <Typography variant="h4" style={style}>Edit</Typography>
                <form>


                    <TextField type="text" label="Nume" fullWidth margin="normal" name="name" value={this.state.name}
                               onChange={this.onChange}/>

                    <TextField type="text" label="Comment" fullWidth margin="normal" name="comment"
                               value={this.state.comment} onChange={this.onChange}/>

                    <Button variant="contained" color="primary" onClick={this.save}>Save</Button>

                </form>
            </div>
        );
    }
}

const style = {
    display: 'flex',
    justifyContent: 'center'
}

export default EditComponent;