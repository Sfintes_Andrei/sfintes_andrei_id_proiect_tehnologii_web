import React, { Component } from 'react'
import ApiService from "../../service/ApiService";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

class AddComponent extends Component{

    constructor(props){
        super(props);
        this.state ={
            id: '',
            name: '',
            comment: ''
        }
        this.save = this.save.bind(this);
    }

    save = (e) => {
        e.preventDefault();
        let doc = {id: this.state.id, name: this.state.name, comment: this.state.comment};
        ApiService.add(doc)
            .then(res => {
                this.setState({message : 'User added successfully.'});
                this.props.history.push('/list');
            });
    }

    upload() {
        this.props.history.push('/upload');
    }


    onChange = (e) =>
        this.setState({ [e.target.name]: e.target.value });

    render() {
        return(
            <div>
                <Typography variant="h4" style={style}>Add</Typography>
                <form style={formContainer}>

                    <TextField type="text" label="Nume" fullWidth margin="normal" name="name" value={this.state.name} onChange={this.onChange}/>

                    <TextField type="text" label="Comment" fullWidth margin="normal" name="comment" value={this.state.comment} onChange={this.onChange}/>

                    <Button variant="contained" color="primary" onClick={this.save}>Save</Button>
                    <Button variant="contained" color="primary" onClick={() => this.upload()}>
                        Upload
                    </Button>
            </form>
    </div>
        );
    }
}
const formContainer = {
    display: 'flex',
    flexFlow: 'row wrap'
};

const style ={
    display: 'flex',
    justifyContent: 'center'

}

export default AddComponent;