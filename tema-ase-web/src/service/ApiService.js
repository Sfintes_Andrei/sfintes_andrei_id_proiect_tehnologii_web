import axios from 'axios';

const API_BASE_URL = 'http://localhost:8080';

class ApiService {

    fetch() {
        return axios.get(API_BASE_URL + "/fetch" );
    }


    delete(doc) {
        return axios.post(API_BASE_URL + '/delete',doc);
    }

    add(doc) {
        return axios.post(API_BASE_URL + '/insert',doc);
    }

    edit(doc) {
        return axios.post(API_BASE_URL + '/update',doc);
    }

}

export default new ApiService();