var mysql = require("mysql");
var db= require("./db_properties");
const Sequelize = require('sequelize');

const sequelize = new Sequelize(db.database, db.user, db.password, {
    host: 'localhost',
    dialect: 'mysql',
    define: {timestamps: false}
});

const doc = sequelize.define('document', {
    name: {
        type: Sequelize.STRING
    },
    uid: {
        type: Sequelize.STRING
    },
    comment: {
        type: Sequelize.STRING
    }
});


const docAudit = sequelize.define('document_audit', {
    documentId: {
        type: Sequelize.STRING
    },
    lastchangetime: {
        type: Sequelize.DATE
    }
});


doc.hasMany(docAudit, {as: 'docAudit_fk', foreignKey: 'documentId'})



docAudit.belongsTo(doc, { as : 'docAudit_fk', foreignKey: 'documentId' });



function docAuditCreate(iddocument){
    docAudit.create({
        documentId:iddocument,
        lastchangetime: new Date()
    });
}

module.exports =
{
    remove: function (id, res) {
        doc.destroy({
            where: {
                id: id
            }
        }).then(function (rowDeleted) {
            if (rowDeleted === 1) {
                res.send('Deleted successfully');
            }
        }, function (err) {
            console.log(err);
        });
    },
    search: function (res) {
        doc.findAll({ include: [{ all: true, nested: true }]}).then(function (recordsArray) {
            res.send(recordsArray);
        });
    },
    insert: function (res,req) {
        var p_name=req.body.name;
        var p_comment=req.body.comment;
        var uuid = require("uuid");
        let uid = uuid.v4();

        doc.create({
            name:p_name,
            uid: uid,
            comment: p_comment
        }).then(function (created) {
            docAuditCreate(created.dataValues.id);
            res.send('Created successfully');
        })

    },
    edit: function (res,req) {
        var id=req.body.id;
        var name=req.body.name;
        var comment=req.body.comment;

        doc.update(
            { name: name , comment: comment},
            { where: { id: id } }
        ).then(function (updated) {
            res.send('Updated successfully');
        })
    }
}
